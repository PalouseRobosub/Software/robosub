#!/bin/bash

#Simple shell script -> xterm -e creates a new xterm terminal and executes and process.
xterm -e ros2 run robosub thruster_maestro --ros-args --params-file ../param/cobalt.yaml & xterm -e ros2 run robosub control --ros-args --params-file ../param/cobalt.yaml & ./objectdetection.sh & ros2 run robosub keyboard_control
