import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import ExecuteProcess
from launch_ros.actions import Node


def generate_launch_description():
    cobalt_urdf_file = os.path.join(get_package_share_directory('robosub'),
                                    'models', 'cobalt.urdf')

    rviz_file = os.path.join(get_package_share_directory('robosub'),
                             'rviz', 'basic.rviz')

    cobalt_urdf = None
    with open(cobalt_urdf_file) as f:
        cobalt_urdf = f.read()

    return LaunchDescription([
        Node(package='robot_state_publisher',
             executable='robot_state_publisher',
             name='robot_state_publisher',
             parameters=[{'robot_description': cobalt_urdf}],
             remappings=[('base_link', 'cobalt')]),

        ExecuteProcess(cmd=['rviz2', '-d', rviz_file], output='screen')
    ])
