import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import ExecuteProcess
from launch_ros.actions import Node


def generate_launch_description():
    cobalt_yaml = os.path.join(get_package_share_directory('robosub'),
                               'param', 'cobalt.yaml')

    return LaunchDescription([
        Node(package='robosub',
             executable='thruster_maestro',
             name='thruster',
             parameters=[cobalt_yaml]),

        ExecuteProcess(cmd=['rqt'], output='screen')
    ])
