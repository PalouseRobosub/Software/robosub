#include "MS5837.h"
#include <Wire.h>

MS5837 depth_sensor;

void setup() {
    Serial.begin(9600);
    Wire.begin();

    while (depth_sensor.init()) {
      Serial.println("Sensor Init Failed");
      delay(1000);
    }

    delay(500);

    depth_sensor.setFluidDensity(997); // 997 kg/m^3 is freshwater, 1029 for seawater.
}

void loop() {
    int depth;
    depth_sensor.read();

    depth = 10000 * depth_sensor.depth();
    Serial.println(depth);

    delay(300);
}
