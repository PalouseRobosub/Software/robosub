# Robosub

[![pipeline status](https://gitlab.com/PalouseRobosub/Software/robosub/badges/master/pipeline.svg)](https://gitlab.com/PalouseRobosub/Software/robosub/commits/master)

Robosub Club of The Palouse consists of students from Washington State University. This club is focused on building an Autonomous Unmanned Vehicle for the Robonation robosub competition.

# Documentation
Documentation can be found on our [robosub wiki](http://robosub.eecs.wsu.edu/wiki/start).
