# simple script to ping all systems on the sub

# function to try pinging a single subsystem
# stops pinging if any ping is succesful
# first argument is the hostname to ping
# second argument is the number of pings to try before giving up
pingSubsystem()
{
  echo pinging $1

  count=0

  while ! ping -c 1 $1 -q 2>&1 >/dev/null
  do 
        count=$(($count+1))
        if test $count -eq $2
        then 
             echo failed to ping $1
             return
        fi 
  done

  echo success
}

echo pinging all systems

pingSubsystem cobalt 3
pingSubsystem ipcam1 3
pingSubsystem ipcam2 3
pingSubsystem ipcam3 3
pingSubsystem jetson 3
pingSubsystem hydro-zynq 3
