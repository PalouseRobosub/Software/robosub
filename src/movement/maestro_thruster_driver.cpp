#include "movement/maestro_thruster_driver.h"
#include <memory>
#include <string>

using namespace std::literals::chrono_literals;

namespace rs
{
    /**
     * Constructor.
     */
    MaestroThrusterDriver::MaestroThrusterDriver() :
        Node("thruster"),
        _is_initialized(false),
        _thruster_timeout_duration(2, 0)
    {
        this->_last_msg_time = this->get_clock()->now();

        if (this->loadAllParams() != 0) {
            RCLCPP_FATAL(this->get_logger(),
                         "critical thruster parameter not found, exiting");
            exit(1);
        }
    }

    /**
     * Deconstructor.
     */
    MaestroThrusterDriver::~MaestroThrusterDriver()
    {
        this->zeroThrusterSpeeds();
    }

    /**
     * initializes the thruster controller. Must be called before using any
     * other functions
     *
     * @param delay_ms Delay specified in milliseconds to continue sending
     *        zeroing signal to continue thruster operation.
     */
    int MaestroThrusterDriver::init(const double post_reset_delay_ms)
    {
        auto serial = std::make_shared<Serial>();
        serial->Open(_thruster_port.c_str(), B9600, this->shared_from_this());

        if(setPort(serial) != 0)
        {
            return -1;
        }

        /*
         * Initialize all data maps to indicate that resets need to occur
         * immediately (for arming signals) and initialize max speed to zero.
         */
        rclcpp::Time now = this->get_clock()->now();
        for(uint8_t i = 0; i < max_thrusters; ++i)
        {
            _next_reset[i] = now;
        }

        _post_reset_delay_ms = post_reset_delay_ms;
        if (post_reset_delay_ms < min_post_reset_delay_ms)
        {
            RCLCPP_ERROR(this->get_logger(),
                         "Thruster millisecond delay is too low.");
            return -1;
        }

        _timeout_timer = rclcpp::create_timer
                         (this->shared_from_this(), this->get_clock(),
                          rclcpp::Duration::from_seconds(0.1),
                         std::bind(&MaestroThrusterDriver::checkTimeout, this));

        _thruster_sub = this->create_subscription<robosub_msgs::msg::Thruster>
                     ("thruster", 1,
                     std::bind(&MaestroThrusterDriver::thrusterCallback, this,
                     std::placeholders::_1));

        _is_initialized = true;

    //    for (int i = 0; i < 35; ++i)
      //      zeroThrusterSpeeds();

        return 0;
    }

    /**
     * Sets the serial port used to communicate with the thrusters.
     *
     * @param port A pointer to a configured and initialized serial port to be
     *        used for communicating with the Maestro.
     *
     * @return Zero upon success and -1 upon failure.
     */
    int MaestroThrusterDriver::setPort(std::shared_ptr<Serial> port)
    {
        _port = port;
        if (_port == nullptr)
        {
            RCLCPP_ERROR(this->get_logger(),
                      "Serial port pointer supplied is not a valid pointer.");
            return -1;
        }

        uint8_t detect_byte = 0xAA;
        if (port->Write(&detect_byte, 1, this->shared_from_this()) != 1)
        {
            RCLCPP_ERROR(this->get_logger(),
                         "Serial port failed to write baud-detection bit.");
            return -1;
        }

        return 0;
    }

    /**
     * Sets the speed of a thruster.
     *
     * @param speeds The normalized speed to set the thruster to.
     *
     * @return 0 on success and -1 on failure.
     */
    int MaestroThrusterDriver::set(double speed, const uint8_t &channel)
    {
        if(_is_initialized != true)
        {
            RCLCPP_ERROR(this->get_logger(),
                      "motor controller used before being initialized");
            return -1;
        }
        
	// this is a temporary dead band that will fix some motors.
	// this function is receiving incorrect speed of 0.020833 when buttons are not being pressed.
	if (speed < 0.0209 && speed > 0.0208) {
		speed = 0;	
	}
	if (speed > -0.0209 && speed < -0.0208) {
		speed = 0;
	}
        /*
         * Create an array of bytes. A thruster command requires 4 bytes
         */
        uint8_t command[4];
        
	// this statement is to track the speed being passed into the set() function
	RCLCPP_INFO(this->get_logger(), std::to_string(speed));

        command[0] = static_cast<uint8_t>(MaestroCommands::SetTarget);
        command[1] = channel;

        /*
         * Check to see if it's currently time to send a reset signal.
         * Send the reset signal first if necessary
         */
        rclcpp::Time now = this->get_clock()->now();
        if (now > _next_reset[channel])
        {
            RCLCPP_DEBUG_STREAM(this->get_logger(),
                                "Sending thruster reset signal.");
            if (parseNormalized(0, command[3], command[2]) < 0)
            {
                RCLCPP_ERROR(this->get_logger(),
                             "Parse Normalized encountered abnormal "
                             "thruster speed.");
                return -1;
            }

            if (_port->Write(command, sizeof(command), this->shared_from_this())
                != sizeof(command))
            {
                RCLCPP_ERROR(this->get_logger(),
                             "Serial port failed to write entire command.");
                return -1;
            }

            _next_reset[channel] = this->get_clock()->now() +
                    rclcpp::Duration::from_seconds(reset_timeout);

            /*
             * Sleep to ensure that the zero pulse has propogated to the ESC.
             * Any value less than 185ms may result in the ESC not receiving
             * the zero pulse, which will cause it to malfunction
             */
            double sleep_period_sec = static_cast<double>(_post_reset_delay_ms)
                                                                        / 1000;
            rclcpp::Rate(1.0 / sleep_period_sec).sleep();
        }

        /*
         * Send thruster command down the to the Maestro.
         */
        if (speed < -1 || speed > 1)
        {
            RCLCPP_ERROR(this->get_logger(),
                         "Thruster speed out of range.");
            return -1;
        }

        const int signal = parseNormalized(speed, command[3], command[2]);

        if (signal < 0)
        {
            RCLCPP_ERROR(this->get_logger(),
                         "Parse Normalized encountered "
                         "abnormal thruster speed.");
            return -1;
        }

        /*
         * The BasicESC has a signal deadband of +/- 25
         * microseconds on the signal pulse. Print out information
         * if the signal is in the dead-band to inform the
         * operator that the thruster should not spin.
         */
        if (signal != 6000 && abs(signal/4 - 1500) < 25)
        {
            RCLCPP_WARN(this->get_logger(),
                        "Parsed signal is in thruster dead-band.");
        }

        /*
         * Write the data down the serial port.
         */
        return (_port->Write(command, sizeof(command), this->shared_from_this())
                != sizeof(command));
    }

    /**
     * Callback for when a message is published to the thruster topic. 
     * Sets thrusters based on the received message.
     */
    void MaestroThrusterDriver::thrusterCallback(
                       const robosub_msgs::msg::Thruster::ConstSharedPtr msg)
    {
        int result = 0;
        for (unsigned int i = 0; i < msg->data.size(); i++)
        {
            result = this->set(msg->data[i], _mThruster_info[i].channel);
            if(result != 0)
            {
                RCLCPP_ERROR_STREAM(this->get_logger(),
                                    "Setting speed of thrusters failed.");
            }
        }
        _last_msg_time = this->get_clock()->now();
    }

    /**
     * Sets all thruster speeds to 0.
     */
    void MaestroThrusterDriver::zeroThrusterSpeeds()
    {
        int result = 0;
        for (unsigned int i = 0; i < _mThruster_info.size(); i++)
        {
            result = this->set(0, _mThruster_info[i].channel);
            if(result != 0)
            {
                RCLCPP_ERROR_STREAM(this->get_logger(),
                                    "Setting speed of thrusters failed.");
            }
        }
    }

    /**
     * Stops the thrusters if no thruster messages have been published for
     * thruster_timeout_duration.
     * For example, if the control system has crashed.
     */
    void MaestroThrusterDriver::checkTimeout()
    {
        RCLCPP_DEBUG(this->get_logger(),
                     "checking when last thruster message was received");
        if (this->get_clock()->now() >
            _last_msg_time + _thruster_timeout_duration)
        {
            RCLCPP_INFO_THROTTLE(this->get_logger(), *(this->get_clock()),
                            _thruster_timeout_duration.nanoseconds()/1000000,
                            "the thruster has timed out!");
            this->zeroThrusterSpeeds();
        }
    }

    /**
     * Parses a normalized thrust value into a Maestro-compatible
     * command.
     *
     * @param normalized_force The normalized speed to parse.
     *        Values must fall within the range [-1,1] and
     *        represent a ratio of the total thruster force
     *        desired.
     * @param[out] msb The location to store the most significant bit
     *             of the result.
     * @param[out] lsb The location to store the least significant bit
     *             of the result.
     *
     * @return The thruster command signal on success and -1 on
     *         failure.
     */
    int MaestroThrusterDriver::parseNormalized(const double normalized_force,
                                                   uint8_t &msb, uint8_t &lsb)
    {
        if (normalized_force < -1 || normalized_force > 1)
        {
            return -1;
        }

        /*
         * To convert the normalized speed into a thruster
         * command, the two characteristic equations found for
         * either positive or negative force need to be applied
         * to the desired thrust output. The characteristic
         * equations take in the thrust as KgF, so convert the
         * normalized thrust value to KgF. The result of this polynomial will
         * be the signal to send to the thruster. Note that
         * thruster signals are centered around 1500, going down
         * to 1100 for negative and up to 1900 for positive
         * signals.
         */
        uint16_t signal = 0;
        double force_kgf = normalized_force * _max_thrust_kgf;

        if (std::fabs(force_kgf) < _minimum_thrust_kgf)
        {
            force_kgf = 0;
        }

        RCLCPP_DEBUG_STREAM(this->get_logger(),
                            "Force (KgF): " << force_kgf);

        if (force_kgf > 0)
        {
            signal = a_positive * pow(force_kgf, 3) +
                     b_positive * pow(force_kgf, 2) +
                     c_positive * force_kgf +
                     d_positive;
        } else if (force_kgf < 0) {
            signal = a_negative * pow(force_kgf, 3) +
                     b_negative * pow(force_kgf, 2) +
                     c_negative * force_kgf +
                     d_negative;
        } else {
            signal = 1500;
        }

        /*
         * Implement a software hard limit on the signals being sent to the
         * thrusters to ensure that they do not draw over 10 Amps of current.
         */
        if (signal > 1780)
        {
            signal = 1780;
            RCLCPP_WARN(this->get_logger(),
                        "Software hard-limitting thrusters forward.");
        } else if (signal < 1230) {
            signal = 1230;
            RCLCPP_WARN(this->get_logger(),
                        "Software hard-limitting thrusters in reverse.");
        }

        /*
         * Until now, the signal has been represented in microseconds.
         * Convert this value to quarter microseconds as specified in the
         * Maestro datasheet.
         */
        signal *= 4;

        /*
         * Store results into supplied locations.
         */
        msb = signal >> 7;
        lsb = signal & 0x7F;

        return signal;
    }

    /**
     * Loads all thruster maestro parameters and stores relevant data in
     * class variables.
     *
     * @return 0 if parameters load successfully
     *        -1 if critical parameters
     */
    int MaestroThrusterDriver::loadAllParams()
    {
        this->declare_parameter("ports.thruster");
        this->declare_parameter("thrusters.timeout");
        this->declare_parameter("thrusters.max_thrust");
        this->declare_parameter("thrusters.names");

        std::vector<std::string> thruster_names;

        if(!this->get_parameter("thrusters.names", thruster_names))
        {
            RCLCPP_ERROR(this->get_logger(), "failed to load thruster names");
            return -1;
        }

        for(auto i = thruster_names.begin(); i < thruster_names.end(); i++)
        {
            this->declare_parameter(std::string("thrusters.mapping.") +
                                    std::string(*i) + std::string(".channel"));
        }

        /*
         * Check if the maestro is running as part of a simulation
         */
        bool simulated = false;
        if(!this->get_parameter("use_sim_time", simulated))
        {
            RCLCPP_INFO(this->get_logger(), "no simulation detected");
        }

        /*
         * Load what port the maestro is connected to
         */
        if(!this->get_parameter("ports.thruster", _thruster_port))
        {
            RCLCPP_ERROR(this->get_logger(),
                         "No serial port specified for thrusters");
            return -1;
        }

        /*
         * get virtual serial port from parameter server this is a simulation
         */
        if(simulated)
        {
            RCLCPP_INFO(this->get_logger(),
                        "simulation detected, acquiring vsp");
            auto param_client = std::make_shared<rclcpp::SyncParametersClient>
                                                (this, "/sim_param_server");
            while(!param_client->wait_for_service(2s))
            {
                RCLCPP_ERROR(this->get_logger(),
                             "unable to query sim param server, retrying...");
            }

            RCLCPP_INFO(this->get_logger(), "connected to param server");
            while(!param_client->has_parameter("vsp_initialized"))
            {
                RCLCPP_INFO(this->get_logger(), "waiting for vsp to start");
                rclcpp::Rate(1).sleep();
            }

            if(!param_client->has_parameter("ports.thruster"))
            {
                RCLCPP_ERROR(this->get_logger(),
                             "failed to get thruster vsp from param server");
                return -1;
            } else {
                _thruster_port = param_client->get_parameter("ports.thruster",
                                                     std::string("/dev/null"));
                RCLCPP_INFO_STREAM(this->get_logger(),
                                   "virtual serial port: " << _thruster_port);
            }
        }

        /*
         * Load thruster timeout or use default of 2 sec if one is not set.
         */ 
        double timeout;
        if(!this->get_parameter("thrusters.timeout", timeout))
        {
            RCLCPP_WARN(this->get_logger(),
                   "no thruster timeout specified, defaulting to 2 seconds");
            timeout = 2.0;
        }
        _thruster_timeout_duration = rclcpp::Duration::from_seconds(timeout);
        RCLCPP_INFO_STREAM(this->get_logger(), "thruster timeout (sec): " <<
                                               timeout);

        /*
         * Load the maximum thruster force and the back thrust
         * ratio from the settings.
         */
        double max_thrust_newtons = 0;
        if (!this->get_parameter("thrusters.max_thrust",
                                       max_thrust_newtons))
        {
            RCLCPP_ERROR(this->get_logger(),
                         "Failed to load maximum thruster force.");
            return -1;
        }

        /*
         * The scaling factor for converting newtons to KgF is
         * 0.101972. Convert the Newtonian thrust into KgF.
         */
        _max_thrust_kgf = max_thrust_newtons * 0.101972;
        RCLCPP_INFO_STREAM(this->get_logger(),
                           "Maximum thrust in KgF: " << _max_thrust_kgf);

        for(auto i = thruster_names.begin(); i < thruster_names.end(); ++i)
        {
            Thruster_info one_thruster;
            one_thruster.name = std::string(*i);
            if(!this->get_parameter(std::string("thrusters.mapping.")
                                    + one_thruster.name
                                    + std::string(".channel"),
                                    one_thruster.channel))
            {
                RCLCPP_ERROR(this->get_logger(),
                             std::string("unable to load channel for ")
                             + one_thruster.name);
                return -1;
            }

            RCLCPP_DEBUG_STREAM(this->get_logger(), one_thruster.name
                                                << " channel: "
                                                << (int) one_thruster.channel);
            _mThruster_info.push_back(one_thruster);
        }

        return 0;
    }
}
