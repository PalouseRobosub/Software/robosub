#ifndef MOVEMENT__CONTROL_SYSTEM_H_
#define MOVEMENT__CONTROL_SYSTEM_H_

#include <eigen3/Eigen/Dense>
#include <cstdint>
#include <string>
#include <vector>
#include <deque>

#include "geometry_msgs/msg/quaternion.h"
#include "geometry_msgs/msg/vector3.hpp"
#include "geometry_msgs/msg/quaternion_stamped.hpp"
#include "geometry_msgs/msg/accel_stamped.hpp"
#include "geometry_msgs/msg/point_stamped.hpp"
#include "robosub_msgs/msg/control.hpp"
#include "robosub_msgs/msg/control_status.hpp"
#include "robosub_msgs/msg/float32_stamped.hpp"
#include "robosub_msgs/msg/thruster.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rotation_engine.hpp"
#include "tf2/transform_datatypes.h"
#include "tf2/LinearMath/Matrix3x3.h"

using namespace Eigen;
using std::string;

namespace robosub
{
class ControlSystem : public rclcpp::Node
{
    /*
     * Define custom types for using Eigen.
     */
    using Vector6d = Matrix<double, 6, 1>;
    using Vector6 = Matrix<int, 6, 1>;
    using Vector12d = Matrix<double, 12, 1>;
    using Matrix6d = Matrix<double, 6, 6>;

    /*
     * Defines conversion scale for converting between radians and degrees.
     */
    static constexpr double _PI_OVER_180 = (3.1415 / 180.0);

    static constexpr double _180_OVER_PI = (180.0 / 3.14159);

    /*
     * Define a custom type for storing a state measurement
     */
    struct StateMeasurement
    {
        StateMeasurement(const rclcpp::Time _time, const double _val) :
            time(_time),
            val(_val)
        {
        }

        /*
         * The time at which the measurement was taken.
         */
        const rclcpp::Time time;

        /*
         * The value of the measurement
         */
        const double val;
    };

public:
    ControlSystem();
    void InputControlMessage(const robosub_msgs::msg::Control::ConstSharedPtr
                             msg);
    void InputOrientationMessage(
            const geometry_msgs::msg::QuaternionStamped::ConstSharedPtr
                  quat_msg);
    void InputLocalizationMessage(
            const geometry_msgs::msg::PointStamped::ConstSharedPtr point_msg);
    void InputDepthMessage(
            const robosub_msgs::msg::Float32Stamped::ConstSharedPtr depth_msg);
    void CheckTimeout();
    void ReloadPIDParams();
    robosub_msgs::msg::Thruster CalculateThrusterMessage();
    robosub_msgs::msg::Thruster GetZeroThrusterMessage();
    robosub_msgs::msg::ControlStatus GetControlStatus();
    geometry_msgs::msg::AccelStamped GetAccelerationEstimate();

    bool isEnabled();
    void setEnabled(bool enable);

private:
    bool enabled;
    void calculate_motor_control();
    double wraparound(double x, double min, double max);
    std::string state_to_string(uint8_t state);
    rclcpp::Time last_msg_time;

    int declareAllParams();
    void declarePIDParams();

    /*
     * Stores the names of all thrusters.
     */
    std::vector<std::string> thruster_names;

    /*
     * Defines the current goals of the submarine in the order of X, Y, PSI,
     * PHI, and THETA.
     */
    Vector6d goals;

    /*
     * Defines the type of goal as specified in the robosub_msgs::control::goal
     * enumeration.
     */
    Matrix<uint8_t, 6, 1> goal_types;

    /*
     * The current status of all the integrals for X, Y, Z, PSI, PHI, and
     * THETA. Elements are stored in that order.
     */
    Vector6d current_error;
    Vector6d current_integral;
    Vector6d current_derivative;

    /*
     * Defines the last calculated non-truncated translation control.
     */
    VectorXd translation_control;

    /*
     * Defines the last calculated non-truncated rotation control.
     */
    VectorXd rotation_control;

    /*
     * Defines the total control message of rotation and translation after
     * truncation has occurred.
     */
    VectorXd total_control;

    /*
     * Defines the control PID parameters and the physical constants of the
     * submarine.
     */
    Vector6d P, I, D;
    Vector6d windup, hysteresis;
    Vector6d sub_mass;
    double buoyancy_offset = 0.0;

    /*
     * Define the motor matrix (inverted) used to solve for the motor commands.
     */
    MatrixXd motors_inverted;

    /*
     * Defines the maximum absolute limit on a translational motor control
     * command.
     */
    double t_lim;

    /*
     * Defines the maximum absolute limit on a rotational motor control
     * command.
     */
    double r_lim;

    /*
     * Defines the maximum thrust a thruster can output in newtons.
     */
    double max_thrust;

    /*
     * Defines the current state of the submarine. This vector stores elements
     * in the following order:
     *      X, Y, Z, Psi (roll), Phi (pitch), Theta (yaw)
     */
    Vector6d state_vector;

    /*
     * Defines the number of thrusters on the submarine.
     */
    int num_thrusters;

    /*
     * Boolean vector indicating that new state measurements are available.
     */
    Matrix<bool, 6, 1> new_measurement_available;

    /*
     * The previous errors calculated.
     */
    Matrix<std::deque<StateMeasurement>, 6, 1> previous_error;

    /*
     * The previously calculated acceleration exerted on the submarine.
     */
    Vector6d acceleration_estimate;
};
}
#endif // MOVEMENT__CONTROL_SYSTEM_H_
