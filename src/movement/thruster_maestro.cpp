#include <rclcpp/rclcpp.hpp>
#include <memory>
#include "movement/maestro_thruster_driver.h"

using namespace rs;

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    auto thrusterController = std::make_shared<MaestroThrusterDriver>();
    thrusterController->init();

    rclcpp::spin(thrusterController);

    return 0;
}
