#include <string>
#include <memory>

#include "geometry_msgs/msg/quaternion.hpp"
#include "geometry_msgs/msg/quaternion_stamped.hpp"
#include "geometry_msgs/msg/accel_stamped.hpp"
#include "movement/control_system.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float32.hpp"
#include "utility/ThrottledPublisher.hpp"
#include "std_srvs/srv/empty.hpp"
#include "std_srvs/srv/set_bool.hpp"


using namespace robosub;

/*
 * The control system object.
 */
std::shared_ptr<ControlSystem> control_system;

/**
 * Specified true if the control system should be silenced.
 */
bool silence_control_system = false;

/*
 * Function to deal with Enable/Disable service call.
 */
bool disable(const std::shared_ptr<std_srvs::srv::Empty::Request> req,
                   std::shared_ptr<std_srvs::srv::Empty::Response> res)
{
    control_system->setEnabled(false);
    return true;
}

/**
 * Service callback for silencing the control system.
 *
 * @param req The request.
 * @param res[out] The result of the request.
 *
 * @return True.
 */
bool toggle(const std::shared_ptr<std_srvs::srv::SetBool::Request> req,
            std::shared_ptr<std_srvs::srv::SetBool::Response> res)
{
    silence_control_system = req->data;
    res->success = true;
    return true;
}

/**
 * Main entry point.
 *
 * @param argc, argv Arguments provided to ROS.
 *
 * @return Zero upon completion.
 */
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    control_system = std::make_shared<ControlSystem>();

    auto depth_sub = control_system->create_subscription
                    <robosub_msgs::msg::Float32Stamped>
                   ("depth", 1,
                    std::bind(&ControlSystem::InputDepthMessage, control_system,
                              std::placeholders::_1));

    auto orientation_sub = control_system->create_subscription
                           <geometry_msgs::msg::QuaternionStamped>
                           ("orientation", 1,
                            std::bind(&ControlSystem::InputOrientationMessage,
                                      control_system, std::placeholders::_1));

    auto localization_sub = control_system->create_subscription
                            <geometry_msgs::msg::PointStamped>
                            ("simulator/cobalt/position", 1,
                             std::bind(&ControlSystem::InputLocalizationMessage,
                                       control_system, std::placeholders::_1));

    auto control_sub = control_system->create_subscription
                       <robosub_msgs::msg::Control>
                       ("control", 1,
                        std::bind(&ControlSystem::InputControlMessage,
                                  control_system, std::placeholders::_1));

    auto thruster_pub = control_system->create_publisher
                                        <robosub_msgs::msg::Thruster>
                                        ("thruster", 1);

    auto accel_pub = control_system->create_publisher
                                     <geometry_msgs::msg::AccelStamped>
                                     ("control_acceleration", 1);

    rs::ThrottledPublisher<robosub_msgs::msg::ControlStatus>
            control_state_pub(std::string("control_status"), 1, 5,
                              control_system);

    rclcpp::TimerBase::SharedPtr timer =
             rclcpp::create_timer(control_system, control_system->get_clock(),
             rclcpp::Duration::from_seconds(0.1),
             std::bind(&ControlSystem::CheckTimeout, control_system));

    rclcpp::Service<std_srvs::srv::Empty>::SharedPtr disableService =
        control_system->create_service<std_srvs::srv::Empty>
        ("control/disable", &disable);

    rclcpp::Service<std_srvs::srv::SetBool>::SharedPtr silentService =
        control_system->create_service<std_srvs::srv::SetBool>
        ("control/silence", &toggle);

    int rate;
    control_system->get_parameter("rate.control", rate);
    rclcpp::Rate r(rate);

    while(rclcpp::ok())
    {
        control_state_pub.publish(control_system->GetControlStatus());
        accel_pub->publish(control_system->GetAccelerationEstimate());
        if(control_system->isEnabled() && !silence_control_system)
        {
            thruster_pub->publish(control_system->CalculateThrusterMessage());
        } else {
            thruster_pub->publish(control_system->GetZeroThrusterMessage());
        }
        rclcpp::spin_some(control_system);
        r.sleep();
    }

    return 0;
}
