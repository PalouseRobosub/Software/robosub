#ifndef CONTROLLERS__GAMEPAD_DRIVER_HPP_
#define CONTROLLERS__GAMEPAD_DRIVER_HPP_

#include <fcntl.h>
#include <linux/joystick.h>
#include <unistd.h>
#include <ros/ros.h>
#include <cstdint>
#include <iostream>
#include <cstdlib>
#include <string>

#include "robosub_msgs/gamepad.hpp" // Gamepad Msg

typedef struct
{
    double axisX;
    double axisY;
    double axisZ;

    double axisRX;
    double axisRY;
    double axisRZ;

    int hatX;
    int hatY;

    bool button[19];

    uint8_t type;
} GAMEPAD_STATE;

class GamepadDriver
{
private:
    //helper functions
    void parse_event();

    //constants
    int AXIS_MAX;
    int fd;
    char name[128];
    uint8_t num_btns;
    js_event e;
    GAMEPAD_STATE gamepad_data;

    ros::NodeHandle *node;

    //loadable parameters
    std::string device;

public:
    robosub_msgs::gamepad GetGamepadMessage();
    void shutdown();

    GamepadDriver(ros::NodeHandle *n);
    ~GamepadDriver() {}
};

#endif // CONTROLLERS__GAMEPAD_DRIVER_HPP_
