#ifndef CONTROLLERS__JOYSTICK_DRIVER_HPP_
#define CONTROLLERS__JOYSTICK_DRIVER_HPP_

#include <fcntl.h>
#include <linux/joystick.h>
#include <unistd.h>
#include <rclcpp/rclcpp.hpp>
#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <string>

#include "robosub_msgs/msg/joystick.hpp" // Joystick Msg

typedef struct
{
    double axisX;
    double axisY;
    double axisZ;

    int hatX;
    int hatY;

    double throttle;

    bool button[12];
} JOYSTICK_STATE;

class JoystickDriver
{
private:
    //helper functions
    void parse_event();
    //void dead_scale(double &value, double deadzone, double scaling_power);

    //constants
    int AXIS_MAX;
    int fd;
    js_event e;
    JOYSTICK_STATE joystick_data;

    ros::NodeHandle *node;

    //loadable parameters
    std::string device;

    double axisXdeadzone;
    double axisYdeadzone;
    double axisZdeadzone;

    double x_scaling_power;
    double y_scaling_power;
    double z_scaling_power;

    double min_depth;
    double max_depth;

public:
    robosub_msgs::joystick GetJoystickMessage();
    void shutdown();

    JoystickDriver(ros::NodeHandle *n);
    ~JoystickDriver() {}
};

#endif // CONTROLLERS__JOYSTICK_DRIVER_HPP_
