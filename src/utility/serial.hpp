#ifndef UTILITY__SERIAL_HPP_
#define UTILITY__SERIAL_HPP_

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <string>
#include <memory>
#include "rclcpp/rclcpp.hpp"

namespace rs
{
class Serial
{
public:
    Serial();
    ~Serial();
    int Open(const char *port_name, int baud_rate,
             std::shared_ptr<rclcpp::Node> node);
    int Close();
    int Write(uint8_t *buf, int num, std::shared_ptr<rclcpp::Node> node);
    int Read(uint8_t *buf, int num, std::shared_ptr<rclcpp::Node> node);
    int QueryBuffer();
    int Flush();

private:
    //private methods
    void Configure(int baud_rate, std::shared_ptr<rclcpp::Node> node);

    //private members
    int m_port_fd;
    std::string m_port_name;
    bool m_is_open;
};
};

#endif // UTILITY__SERIAL_HPP_
