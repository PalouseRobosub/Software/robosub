#ifndef UTILITY__TEST_TOOLS_HPP_
#define UTILITY__TEST_TOOLS_HPP_

#include <vector>
#include <string>
#include <chrono>
#include <memory>
#include "rclcpp/rclcpp.hpp"

using namespace std::chrono_literals;

namespace rs
{
//this class makes it easy to fork then exec a new process. It will
//automatically send SIGTERM to the child when the class goes out of scope.
class SpawnProcess
{
public:
    SpawnProcess();
    ~SpawnProcess();
    void Spawn(std::shared_ptr<rclcpp::Node> node, std::string cmd,
               std::string args = "");

private:
    pid_t m_pid;
    bool spawned;
};

//this class generates two serial ports that are connected to each other,
//designed for testing programs that talk over a serial port
class SerialTB
{
public:
    SerialTB();
    ~SerialTB();

    //Start() takes in the port the UUT uses, and returns
    //the name of the testing port to use
    int Start(std::string &test_port, std::string &uut_port,
              std::shared_ptr<rclcpp::Node> node);

private:
    SpawnProcess socat_proc;
    bool started;
};


void wait_for_param(const char *param_name, int timeout_seconds = -1);
//void wait_for_subscriber(ros::Publisher pub, int timeout_seconds = -1);
//void wait_for_publisher(ros::Subscriber pub, int timeout_seconds = -1);

template <class msgType>
void wait_for_subscriber(typename rclcpp::Publisher<msgType>::SharedPtr pub,
                         std::shared_ptr<rclcpp::Node> node,
                         int timeout_seconds = -1)
{
    rclcpp::Time exit_time = timeout_seconds == -1 ? rclcpp::Time::max() :
                            node->get_clock()->now() +
                            rclcpp::Duration(timeout_seconds);
    while(pub->get_subscription_count() == 0)
    {
        RCLCPP_DEBUG_THROTTLE(node->get_logger(), *node->get_clock(), 1,
                              "waiting for subscriber on: %s",
                              pub->get_topic_name());
        if(node->get_clock()->now() < exit_time)
        {
            rclcpp::sleep_for(100ms);
        } else { //we timed out, time to die!
            RCLCPP_FATAL(node->get_logger(),
                         "timed out waiting for subscriber: %s",
                         pub->get_topic_name());
            exit(1);
        }
    }
}

template <class msgType>
void wait_for_publisher(typename rclcpp::Subscription<msgType>::SharedPtr sub,
                        int timeout_seconds = -1);

//this class is meant to easy testing nodes, it acquires and holds onto
//received messages
template <class mtype> class SubscriberTB
{
public:
    SubscriberTB()
    {}
    ~SubscriberTB()
    {}
    void Init(std::string topic)
    {
        std::shared_ptr<rclcpp::Node> node =
                        rclcpp::Node::make_shared("subscriberTB_" + topic);

        //m_sub = n.subscribe(topic.c_str(), 1,
        //                    &SubscriberTB<mtype>::Callback, this);
        m_sub = node->create_subscription<mtype>(topic, 1,
        std::bind(&SubscriberTB<mtype>::Callback, this, std::placeholders::_1));
    }
    void Callback(const typename mtype::ConstPtr& msg)
    {
        m_msgs.push_back(*msg);
    }

    mtype GetLatestMsg()
    {
        return m_msgs.back();
    }

    mtype GetAllMsgs()
    {
        return m_msgs;
    }

    void ClearMsgs()
    {
        m_msgs.clear();
    }

private:
    std::vector<mtype> m_msgs;
    //ros::Subscriber m_sub;
    typename rclcpp::Subscription<mtype>::SharedPtr m_sub;
};

//this class is similar to the SubscriberTB, but is meant for analyzing a
//1-D stream of data. The format_callback should be a function that takes
//in the message datatype, and returns 1 double value, which is stored and
//later analyzed.
template <class mtype> class SubscriberAnalyzer
{
public:
    SubscriberAnalyzer()
    {}
    ~SubscriberAnalyzer()
    {}
    void Init(std::string topic,
              double (*format_callback)(const typename mtype::ConstPtr))
    {
        m_node = rclcpp::Node::make_shared("subAnalyzer_" + topic);

        //m_sub = n.subscribe(topic.c_str(), 1,
        //                    &SubscriberAnalyzer<mtype>::Callback, this);
        m_sub = m_node->create_subscription<mtype>(topic, 1,
                        std::bind(&SubscriberAnalyzer<mtype>::Callback, this,
                                    std::placeholders::_1));

        m_format_function = format_callback;

        m_enabled = false;
    }

    void Callback(const typename mtype::ConstPtr msg)
    {
        if(m_enabled == true)
        {
            double data = m_format_function(msg);
            m_data.push_back(data);
        }
    }

    void Start()
    {
        m_enabled = true;
    }

    void Stop()
    {
        m_enabled = false;
    }

    void ClearData()
    {
        m_data.clear();
    }

    double GetMin()
    {
        if(m_data.size() == 0)
        {
            RCLCPP_ERROR(m_node->get_logger(),
                         "no data collected, can't calculate min!");
            return 0.0;
        }

        double min = m_data[0];
        for (unsigned int i = 1; i < m_data.size(); ++i)
        {
            if (m_data[i] < min)
            {
                min = m_data[i];
            }
        }

        return min;
    }

    double GetMax()
    {
        if(m_data.size() == 0)
        {
            RCLCPP_ERROR(m_node->get_logger(),
                         "no data collected, can't calculate max!");
            return 0.0;
        }

        double max = m_data[0];
        for (unsigned int i = 1; i < m_data.size(); ++i)
        {
            if (m_data[i] > max)
            {
                max = m_data[i];
            }
        }

        return max;
    }

    double GetAverage()
    {
        if(m_data.size() == 0)
        {
            RCLCPP_ERROR(m_node->get_logger(),
                         "no data collected, can't calculate average!");
            return 0.0;
        }

        double sum = 0.0;
        for (unsigned int i = 0; i < m_data.size(); ++i)
        {
                sum += m_data[i];
        }

        return sum/m_data.size();
    }

    double GetStandardDeviation()
    {
        if(m_data.size() == 0)
        {
            RCLCPP_ERROR(m_node->get_logger(), "no data collected, "
                      "can't calculate standard deviation!");
            return 0.0;
        }

        double average = GetAverage();
        double sum = 0.0;
        for (unsigned int i = 0; i < m_data.size(); ++i)
        {
            sum += pow(m_data[i] - average, 2);
        }

        return sqrt(sum/m_data.size());
    }

private:
    std::vector<double> m_data;
    typename rclcpp::Subscription<mtype>::SharedPtr m_sub;
    std::shared_ptr<rclcpp::Node> m_node;
    double (*m_format_function)(const typename mtype::ConstPtr);
    bool m_enabled;
};
};

#endif // UTILITY__TEST_TOOLS_HPP_
