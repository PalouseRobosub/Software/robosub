#include <unistd.h>
#include <string>
#include <memory>
#include "rclcpp/rclcpp.hpp"
#include "utility/test_tools.hpp"

namespace rs
{
    SerialTB::SerialTB()
    {
        started = false;
    }

    SerialTB::~SerialTB()
    {
    }

    int SerialTB::Start(std::string &test_port, std::string &uut_port,
                        std::shared_ptr<rclcpp::Node> node)
    {
        if(started == true)
        {
            RCLCPP_ERROR(node->get_logger(),
                         "cannot start serial testbench, already started");
            return -1;
        }
        pid_t pid = getpid();
        test_port = "test_" + std::to_string(pid);
        uut_port = "uut_" + std::to_string(pid);
        std::string args = "pty,raw,echo=0,link=" + test_port +
               " pty,raw,echo=0,link=" + uut_port;
        socat_proc.Spawn(node, "/usr/bin/socat", args.c_str());

        started = true;

        return 0;
    }

};
