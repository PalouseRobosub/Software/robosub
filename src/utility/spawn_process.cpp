#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string>
#include <memory>
#include "rclcpp/rclcpp.hpp"
#include "utility/test_tools.hpp"

namespace rs
{
    SpawnProcess::SpawnProcess()
    {
        m_pid = -1;
        spawned = false;
    }
    void SpawnProcess::Spawn(std::shared_ptr<rclcpp::Node> node,
                             std::string cmd, std::string args)
    {
        if(spawned == true) //if we've already forked our process
        {
            RCLCPP_ERROR(node->get_logger(),
                         "cannot spawn process, already spawned");
            return;
        }

        m_pid = fork();
        if(m_pid < 0) //fork failed
        {
            RCLCPP_FATAL_STREAM(node->get_logger(),
                                "failed to fork, error: " << strerror(errno));
            exit(1);
        } else if (m_pid  == 0) { //child
            RCLCPP_DEBUG(node->get_logger(), "spawning \"%s\" with args \"%s\"",
                      cmd.c_str(), args.c_str());
            char *arg_ptrs[64];

            arg_ptrs[0] = const_cast<char*>(cmd.c_str());
            int i = 1;
            if(args != "")
            {
                arg_ptrs[i] = strtok(const_cast<char*>(args.c_str()), " ");
                while((arg_ptrs[++i] = strtok(NULL, " ")))
                {
                    //this loop body is intended to be empty, all logic happens
                    //in the conditional of the while loop
                }
            }
            arg_ptrs[i] = 0;

            execvp(cmd.c_str(), arg_ptrs);
            RCLCPP_FATAL_STREAM(node->get_logger(),
                             "failed to exec \"" << cmd << "\", error: " <<
                             strerror(errno));
            exit(1);
        } else { //parent
            RCLCPP_DEBUG(node->get_logger(), "Forked child, pid: %d", m_pid);
            //nothing to do
        }
        spawned = true;
    }

    SpawnProcess::~SpawnProcess()
    {
        //kill the child process when we go out of scope
        kill(m_pid, SIGTERM);
    }

};
