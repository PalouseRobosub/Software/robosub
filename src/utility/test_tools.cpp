#include "utility/test_tools.hpp"
#include <chrono>
#include <memory>

using namespace std::chrono_literals;

namespace rs
{
void wait_for_param(const char *param_name, int timeout_seconds,
                    std::shared_ptr<rclcpp::Node> node)
{
    rclcpp::Time exit_time = timeout_seconds == -1 ? rclcpp::Time::max() :
                            node->get_clock()->now() +
                            rclcpp::Duration(timeout_seconds);
    while(node->has_parameter(param_name) == false)
    {
        RCLCPP_DEBUG_THROTTLE(node->get_logger(), *node->get_clock(), 1,
                              "waiting for parameter: %s", param_name);
        if(node->get_clock()->now() < exit_time)
        {
            rclcpp::sleep_for(100ms);
        } else { //we timed out, time to die!
            RCLCPP_FATAL(node->get_logger(),
                         "timed out waiting for parameter: %s", param_name);
            exit(1);
        }
    }
}

};
