#ifndef UTILITY__THROTTLEDPUBLISHER_HPP_
#define UTILITY__THROTTLEDPUBLISHER_HPP_

#include <rclcpp/rclcpp.hpp>
#include <string>
#include <memory>

namespace rs
{
template <class msg> class ThrottledPublisher
{
    private:
        rclcpp::Time nextPubTime;
        rclcpp::Duration rate;
        std::shared_ptr<rclcpp::Node> node;
        typename rclcpp::Publisher<msg>::SharedPtr pub;

        std::string paramName;
        double paramHz;
        double lastParamHz;

    public:
        ThrottledPublisher() : rate(0) {}

        ThrottledPublisher(std::string topicName, int queueSize, float hz,
                std::shared_ptr<rclcpp::Node> n, std::string _paramName = "")
        : rate(0)
        {
            node = n;
            pub = node->create_publisher<msg>(topicName, queueSize);
            setRate(hz);

            paramName = _paramName;
            if(paramName != "")
            {
                node->declare_parameter(paramName);
            }
            getRateParam();
        }

        ~ThrottledPublisher()
        {}

        void publish(msg message)
        {
            getRateParam();

            if (nextPubTime <= node->get_clock()->now())
            {
                pub->publish(message);
                nextPubTime = node->get_clock()->now() + rate;
            }
        }

        void getRateParam()
        {
            if(paramName != "")
            {
                if (!node->get_parameter(paramName, paramHz))
                {
                    RCLCPP_ERROR_STREAM(node->get_logger(),
                    "Failed to load param %s." << paramName);
                    return;
                }

                if (paramHz != lastParamHz)
                {
                    RCLCPP_INFO_STREAM(node->get_logger(),
                                       paramName << " parameter changed!");
                    setRate(paramHz);
                    lastParamHz = paramHz;
                }
            }
        }

        void setRate(float hz)
        {
            if(hz != 0.0)
            {
                this->rate = rclcpp::Duration::from_seconds(1.0/hz);
            } else {
                this->rate = rclcpp::Duration::from_seconds(0);
            }
            nextPubTime = this->node->get_clock()->now();
        }
};
};

#endif // UTILITY__THROTTLEDPUBLISHER_HPP_
