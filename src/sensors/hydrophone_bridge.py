#!/usr/bin/python
"""
@author Ryan Summers
@date 2-28-2018

@brief Provides a bridge from the raw socket API implemented on the HydroZynq to
       the ROS infrastructure.
"""

import re
import rclpy
from rclpy.node import Node
import socket
import struct
import threading

from std_srvs.srv import SetBool
from robosub_msgs.msg import HydrophoneDeltas
from robosub.srv import SetInt


class DeltaPacket:
    """ Parses a string for the hydrophone delta result.

    Attributes
        x: The delay in seconds of the first channel.
        y: The delay in seconds of the second channel.
        z: The delay in seconds of the third channel.
    """

    def __init__(self, data):
        # The result string is human readable in the form:
        #   Result - 1: [time in ns] 2: [time in ns] 3: [time in ns]
        matches = re.search(r'.*1: (-?\d+) 2: (-?\d+) 3: (-?\d+).*', data)
        if not matches:
            raise Exception('Invalid result string')

        if len(matches.groups()) != 3:
            raise Exception('Invalid number of groups')

        # results in nanoseconds.
        self.x = float(matches.group(1))
        self.y = float(matches.group(2))
        self.z = float(matches.group(3))


class HydroNode(Node):
    """ Handles bridge tasks for the hydrophone node.

    Attributes
        running: Boolean that specifies true if the threads should continue.
        hostname: The hostname of Cobalt
        zynq_hostname: The hostname of the Zynq processor.
        delta_pub: A ROS publisher for hydrophone delays.
        silence_thread: A thread for silencing the control system.
        stdout_thread: A thread for reading the Zynq STDOUT.
        result_thread: A thread for reading the Zynq hydrophone delays.
        ping_threshold_service: A thread for setting the Zynq ping threshold.
    """

    def __init__(self):
        """ Initializes the node.
        """
        super().__init__('hydrophone_bridge')
        self.running = False
        self.hostname = self.get_parameter_or('~hostname',
                                              alternative_value='192.168.0.2')
        self.zynq_hostname = self.get_parameter_or(
                                                '~zynq_hostname',
                                                alternative_value='192.168.0.7')
        self.threshold = self.get_parameter_or('~threshold',
                                               alternative_value=500)

        self.delta_pub = self.create_publisher(
                HydrophoneDeltas, 'hydrophones/delta', 10)

        self.silence_thread = threading.Thread(
                target=self._silence_thread_target,
                name='Control Silencer Thread')

        self.stdout_thread = threading.Thread(
                target=self._stdout_thread_target,
                name='ROS STDOUT Thread')

        self.result_thread = threading.Thread(
                target=self._result_thread_target,
                name='Result Thread')

        self.ping_threshold_service = self.create_service(
                SetInt,
                'hydrophone_bridge/set_ping_threshold',
                self.set_threshold)
        req = SetInt.Request()
        req.data = self.threshold
        resp = SetInt.Response()
        self.set_threshold(req, resp)

        self.silence_req = SetBool.Request()

    def begin(self):
        """ Start all threads. """
        self.running = True
        self.silence_thread.start()
        self.stdout_thread.start()
        self.result_thread.start()

    def end(self):
        """ Stop all threads. """
        self.running = False
        self.get_logger().info('Terminating threads...')
        self.silence_thread.join()
        self.stdout_thread.join()
        self.result_thread.join()

    def set_threshold(self, req, resp):
        """ ROS Service callback for setting ping threshold. """
        arg_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        arg_sock.connect((self.zynq_hostname, 3000))

        command = 'threshold:{}'.format(req.data)
        arg_sock.send(command.encode())
        arg_sock.close()
        resp.success = True
        return resp

    def _silence_thread_target(self):
        """ Handles requests for silencing thrusters. """
        self.get_logger().info('BRIDGE: Waiting for control/silence service.')
        self.silence_srv_client = self.create_client(SetBool, "control/silence")
        while not self.silence_srv_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info(
               'BRIDGE: unable to acquire control/silence service, retrying...')
        self.get_logger().info('BRIDGE: Service acquired.')

        silence_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        silence_sock.settimeout(0.5)
        silence_sock.bind((self.hostname, 3005))

        while rclpy.ok() and self.running:
            try:
                data = silence_sock.recv(1024)
            except socket.timeout:
                continue

            recv_time = self.get_clock().now()
            if len(data) != 8:
                rclpy.get_logger().warn('Received invalid packet size.')
                continue

            when, duration = struct.unpack('<ii', data)

            while self.get_clock().now() < recv_time + (when / 1000.0):
                continue

            rclpy.get_logger().info('Silencing thrusters for ping.')

            self.silence_req.data = True
            self.silence_srv_client.call(self.silence_req)

            shutdown_time = self.get_clock().now()
            while self.get_clock().now() < shutdown_time + (duration / 1000.0):
                continue

            self.silence_req.data = False
            self.silence_srv_client.call(self.silence_req)
            self.get_logger().info('Disabling silence')
        self.get_logger().info('Silence thread terminating...')

        silence_sock.close()

    def _stdout_thread_target(self):
        """ Handles displaying the Zynq standard output. """
        stdout_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        stdout_sock.settimeout(0.5)
        stdout_sock.bind((self.hostname, 3004))

        while rclpy.ok() and self.running:
            try:
                line = stdout_sock.recv(1024).rstrip('\n')
            except socket.timeout:
                continue

            self.get_logger().info(line.decode())
        self.get_logger().info('STDOUT thread terminating...')
        stdout_sock.close()

    def _result_thread_target(self):
        """ Handles receiving the Zynq's hydrophone delay results. """
        result_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        result_sock.settimeout(0.5)
        result_sock.bind((self.hostname, 3002))

        while rclpy.ok() and self.running:
            try:
                data = result_sock.recv(1024)
            except socket.timeout:
                continue

            try:
                deltas = DeltaPacket(data)
            except Exception as e:
                self.get_logger().warn(
                        'Received invalid HydroZynq datagram: {}'.format(e))
                continue

            msg = HydrophoneDeltas()

            msg.header.stamp = self.get_clock().now()
            msg.header.frame_id = 'hydrophone_array'
            msg.xDelta = rclpy.Duration(nanoseconds=deltas.x).to_msg()
            msg.yDelta = rclpy.Duration(nanoseconds=deltas.y).to_msg()
            msg.zDelta = rclpy.Duration(nanoseconds=deltas.z).to_msg()

            self.delta_pub.publish(msg)

        self.get_logger().info('Result thread terminating...')
        result_sock.close()


if __name__ == '__main__':
    rclpy.init()

    node = HydroNode()

    node.begin()

    rclpy.spin(node)
    node.end()
