import rclpy
from robosub_msgs.msg import Float32Stamped


def depth_callback(msg):
    print(msg.data)


if __name__ == "__main__":
    rclpy.init()
    node = rclpy.node.Node("subscriber_test")
    node.create_subscription(Float32Stamped, "depth", depth_callback, 7)
    rclpy.spin(node)
