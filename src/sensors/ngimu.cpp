/*
 * This file is a modified version of the arduino code found at
 * https://github.com/xioTechnologies/NGIMU-C-Cpp-Example
 */

#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <iostream>
#include <memory>
#include "geometry_msgs/msg/quaternion_stamped.hpp"
#include "robosub_msgs/msg/euler.hpp"
#include "utility/Osc99/Osc99.h"
#include "utility/serial.hpp"
#include "sensors/NgimuReceive.h"
#include "rclcpp/rclcpp.hpp"

std::shared_ptr<rclcpp::Node> ngimu_node;

rclcpp::Publisher<geometry_msgs::msg::QuaternionStamped>::SharedPtr
                                                          ngimu_publisher;

rclcpp::Publisher<robosub_msgs::msg::Euler>::SharedPtr ngimu_pretty_publisher;

// This function is called each time there is a receive error
// function muted so it does nothing
void ngimuReceiveErrorCallback(const char* const errorMessage)
{
    // std::cout << errorMessage << std::endl;
}

// This function is called each time a "/sensors" message is received
// function muted so it does nothing
void ngimuSensorsCallback(const NgimuSensors ngimuSensors)
{
 /*
    std::cout << "/sensors, ";
    std::cout << ngimuSensors.gyroscopeX;
    std::cout << ", ";
    std::cout << ngimuSensors.gyroscopeY;
    std::cout << ", ";
    std::cout << ngimuSensors.gyroscopeZ;
    std::cout << ", ";
    std::cout << ngimuSensors.accelerometerX;
    std::cout << ", ";
    std::cout << ngimuSensors.accelerometerY;
    std::cout << ", ";
    std::cout << ngimuSensors.accelerometerZ;
    std::cout << ", ";
    std::cout << ngimuSensors.magnetometerX;
    std::cout << ", ";
    std::cout << ngimuSensors.magnetometerY;
    std::cout << ", ";
    std::cout << ngimuSensors.magnetometerZ;
    std::cout << ", ";
    std::cout << ngimuSensors.barometer;
    std::cout << std::endl;
    */
}

// This function is called each time a "/quaternion" message is received
void ngimuQuaternionCallback(const NgimuQuaternion ngimuQuaternion)
{
    /*
    std::cout << "/quaternion, ";
    std::cout << ngimuQuaternion.w;
    std::cout << ", ";
    std::cout << ngimuQuaternion.x;
    std::cout << ", ";
    std::cout << ngimuQuaternion.y;
    std::cout << ", ";
    std::cout << ngimuQuaternion.z;
    std::cout << std::endl;
    */

    geometry_msgs::msg::QuaternionStamped ngimu_msg;

    tf2::Quaternion quat(ngimuQuaternion.x, ngimuQuaternion.y,
                         ngimuQuaternion.z, ngimuQuaternion.w);

    double roll, pitch, yaw;
    tf2::Matrix3x3(quat).getRPY(roll, pitch, yaw);

    yaw = -yaw; // invert yaw to match robosub expected values

    quat.setRPY(roll, pitch, yaw);

    ngimu_msg.quaternion.x = quat.getX();
    ngimu_msg.quaternion.y = quat.getY();
    ngimu_msg.quaternion.z = quat.getZ();
    ngimu_msg.quaternion.w = quat.getW();

    roll *= 180/3.14159265358979;
    pitch *= 180/3.14159265358979;
    yaw *= 180/3.14159265358979;

    robosub_msgs::msg::Euler ngimu_euler_msg;

    ngimu_euler_msg.roll = roll;
    ngimu_euler_msg.pitch = pitch;
    ngimu_euler_msg.yaw = yaw;

    ngimu_msg.header.stamp = ngimu_node->get_clock()->now();

    ngimu_publisher->publish(ngimu_msg);
    ngimu_pretty_publisher->publish(ngimu_euler_msg);
}

// This function is called each time a "/euler" message is received.
// function muted so it does nothing
void ngimuEulerCallback(const NgimuEuler ngimuEuler)
{
    /*
    std::cout << "/euler, ";
    std::cout << ngimuEuler.roll;
    std::cout << ", ";
    std::cout << ngimuEuler.pitch;
    std::cout << ", ";
    std::cout << ngimuEuler.yaw;
    std::cout << std::endl;
    */
}

void setupNgimu()
{
    // Initialise NGIMU receive module
    NgimuReceiveInitialise();

    // Assign NGIMU receive callback functions
    NgimuReceiveSetReceiveErrorCallback(ngimuReceiveErrorCallback);
    NgimuReceiveSetSensorsCallback(ngimuSensorsCallback);
    NgimuReceiveSetQuaternionCallback(ngimuQuaternionCallback);
    NgimuReceiveSetEulerCallback(ngimuEulerCallback);
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    ngimu_node = rclcpp::Node::make_shared("ngimu");

    ngimu_publisher = ngimu_node->create_publisher
                                  <geometry_msgs::msg::QuaternionStamped>
                                  ("ngimu/orientation", 1);
    ngimu_pretty_publisher = ngimu_node->create_publisher
                             <robosub_msgs::msg::Euler>
                             ("ngimu/pretty/orientation", 1);

    rs::Serial serial;
    serial.Open("/dev/ttyACM3", 115200, ngimu_node);

    setupNgimu();

    uint8_t data;

    while(rclcpp::ok())
    {
        while(serial.Read(&data, 1, ngimu_node) > 0)
        {
            NgimuReceiveProcessSerialByte(data);
        }
    }

    serial.Close();
    rclcpp::spin(ngimu_node);
    return 0;
}
