#include <eigen3/Eigen/Dense>
#include <geometry_msgs/msg/point.hpp>
#include <geometry_msgs/msg/quaternion.hpp>
#include <visualization_msgs/msg/marker.hpp>
#include <vector>
#include <memory>
#include "robosub_msgs/msg/hydrophone_deltas.hpp"
#include "geometry_msgs/msg/vector3_stamped.hpp"
#include "rclcpp/rclcpp.hpp"
#include "robosub_msgs/msg/vector_spherical.hpp"


//using std::vector;
using Eigen::Vector3d;
//using Eigen::Quaterniond;

std::shared_ptr<rclcpp::Node> node;

rclcpp::Publisher<geometry_msgs::msg::Vector3Stamped>::SharedPtr bearing_pub;
rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr marker_pub;
rclcpp::Publisher<robosub_msgs::msg::VectorSpherical>::SharedPtr spherical_pub;
Vector3d hydrophone_positions;


/*
 * The speed of sound in water is 1484 m/s.
 */
static constexpr double speed_sound_in_water = 1484.0;


// callback for incoming ping time deltas from the hydrophones
// principle of operation: JFM
// (see http://robosub.eecs.wsu.edu/wiki/cs/hydrophones/pinger_bearing/start
// for details)
void deltaCallback(
                const robosub_msgs::msg::HydrophoneDeltas::ConstSharedPtr msg)
{
    Vector3d bearing, d, time_deltas;

    #define NS_IN_SEC 1000000000
    time_deltas[0] = msg->x_delta.sec +
                     static_cast<double>(msg->x_delta.nanosec) / NS_IN_SEC;
    time_deltas[1] = msg->y_delta.sec +
                     static_cast<double>(msg->y_delta.nanosec) / NS_IN_SEC;
    time_deltas[2] = msg->z_delta.sec +
                     static_cast<double>(msg->z_delta.nanosec) / NS_IN_SEC;

    d = time_deltas * speed_sound_in_water;

    bearing = d.cwiseQuotient(hydrophone_positions);

    geometry_msgs::msg::Vector3Stamped bearing_msg;
    robosub_msgs::msg::VectorSpherical spherical_msg;

    bearing_msg.header.stamp = node->get_clock()->now();
    bearing_msg.vector.x = -bearing[0];
    // flip x and y, was spitting out incorrect values
    bearing_msg.vector.y = -bearing[1];

    bearing_msg.vector.z = bearing[2];

    double mag = std::sqrt(bearing[0] * bearing[0] +
                           bearing[1] * bearing[1] +
                           bearing[2] * bearing[2]);
    double theta = atan2(bearing[1], bearing[0]) * 180.0 / M_PI;
    double phi = atan2(sqrt(bearing[1] * bearing[1] + bearing[0] * bearing[0]),
                            bearing[2]) * 180.0 / M_PI;

    spherical_msg.r = mag;
    spherical_msg.theta = theta;// yaw
    spherical_msg.phi = phi;    // pitch

    RCLCPP_DEBUG(node->get_logger(), "vector magnitude: %lf", mag);

    visualization_msgs::msg::Marker marker;
    marker.header.frame_id = "cobalt";
    marker.header.stamp = node->get_clock()->now();
    marker.ns = "pinger_bearing";
    marker.id = 0;
    marker.type = visualization_msgs::msg::Marker::ARROW;
    marker.action = visualization_msgs::msg::Marker::ADD;
    geometry_msgs::msg::Point tail, tip;
    tail.x = tail.y = tail.z = 0;
    tip.x = bearing[0];
    tip.y = bearing[1];
    tip.z = bearing[2];
    marker.points.push_back(tail);
    marker.points.push_back(tip);
    marker.color.a = 1.0;
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;
    marker.scale.x = 0.1;
    marker.scale.y = 0.2;

    bearing_pub->publish(bearing_msg);
    marker_pub->publish(marker);
    spherical_pub->publish(spherical_msg);
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    node = rclcpp::Node::make_shared("pinger_bearing");

    node->declare_parameter("hydrophones.positions.x");
    node->declare_parameter("hydrophones.positions.y");
    node->declare_parameter("hydrophones.positions.z");

    if(!node->get_parameter("hydrophones.positions.x", hydrophone_positions[0]))
    {
        RCLCPP_FATAL(node->get_logger(),
                     "Failed to load X hydrophone position.");
        return -1;
    }

    if(!node->get_parameter("hydrophones.positions.y", hydrophone_positions[1]))
    {
        RCLCPP_FATAL(node->get_logger(),
                     "Failed to load Y hydrophone position.");
        return -1;
    }

    if(!node->get_parameter("hydrophones.positions.z", hydrophone_positions[2]))
    {
        RCLCPP_FATAL(node->get_logger(),
                     "Failed to load Z hydrophone position.");
        return -1;
    }


    auto delta_sub = node->create_subscription
                           <robosub_msgs::msg::HydrophoneDeltas>
                           ("hydrophones/delta", 1, deltaCallback);
    bearing_pub = node->create_publisher<geometry_msgs::msg::Vector3Stamped>
                                        ("hydrophones/bearing", 1);
    marker_pub = node->create_publisher<visualization_msgs::msg::Marker>
                                       ("rviz/hydrophones/bearing", 1);
    spherical_pub = node->create_publisher<robosub_msgs::msg::VectorSpherical>
                                          ("vectorSphericalCoordinates", 1);

    rclcpp::spin(node);

    return 0;
}
