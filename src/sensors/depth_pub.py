#!/usr/bin/python

import rclpy
from rclpy.node import Node
from robosub_msgs.msg import Float32Stamped
import serial

if __name__ == "__main__":
    rclpy.init()
    port = '/dev/ttyACM2'   # arduino port

    ard = serial.Serial(port, 9600, timeout=5)  # ard = arduino
    node = Node("depth_node")
    depth_pub = node.create_publisher(Float32Stamped, 'depth', 1)
    while True:
        try:
            ard.flush()
            msg = ard.readline()        # read from arduino
            msg = msg.decode('utf-8')   # decode the bytes into utf-8 format

            # data comes in format "depth;number", split it so
            # you only have depth
            depth = msg.split(';')[0]
            depth_f = float(depth)      # cast as float

            # divide by 10,000 since it was multiplied
            # by 10,000 before sending as int
            depth_f = depth_f / 10000
            depth_f = depth_f + 0.85

            f32 = Float32Stamped()      # initialize message
            f32.data = depth_f
            f32.header.stamp = node.get_clock().now().to_msg()  # set timestamp

            # node.get_logger().info(depth_f)      # print message to terminal
            depth_pub.publish(f32)       # publish message
        except KeyboardInterrupt:   # so it will still break on ctrl + C
            break
        except Exception as e:      # print the exception
            print(str(e))
