#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <string>
#include <memory>
#include "geometry_msgs/msg/quaternion_stamped.hpp"
#include "geometry_msgs/msg/vector3_stamped.hpp"
#include "robosub_msgs/msg/euler.hpp"
#include "rclcpp/rclcpp.hpp"

#define _180_OVER_PI (180.0 / 3.14159)

rclcpp::Publisher<geometry_msgs::msg::QuaternionStamped>::SharedPtr
                                                  quaternion_publisher;
rclcpp::Publisher<robosub_msgs::msg::Euler>::SharedPtr euler_publisher;
rclcpp::Publisher<geometry_msgs::msg::Vector3Stamped>::SharedPtr
                                                  acceleration_publisher;

robosub_msgs::msg::Euler quaternion_to_euler(
               const geometry_msgs::msg::QuaternionStamped::ConstSharedPtr &msg)
{
    double roll, pitch, yaw;
    tf2::Quaternion tf_quaternion;
    robosub_msgs::msg::Euler euler_msg;

    tf2::fromMsg(msg->quaternion, tf_quaternion);

    tf2::Matrix3x3 m(tf_quaternion);
    m.getRPY(roll, pitch, yaw);

    euler_msg.roll = roll * _180_OVER_PI;
    euler_msg.pitch = pitch * _180_OVER_PI;
    euler_msg.yaw = yaw * _180_OVER_PI;

    return euler_msg;
}

void orientation_callback(
                const geometry_msgs::msg::QuaternionStamped::ConstSharedPtr msg)
{
    quaternion_publisher->publish(*msg);
    euler_publisher->publish(quaternion_to_euler(msg));
}

void acceleration_callback(
                   const geometry_msgs::msg::Vector3Stamped::ConstSharedPtr msg)
{
    acceleration_publisher->publish(*msg);
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> imu_node = rclcpp::Node::make_shared("imu");

    imu_node->declare_parameter("active_imu");

    std::string active_imu;
    if(!imu_node->get_parameter("active_imu", active_imu))
    {
        RCLCPP_FATAL(imu_node->get_logger(), "active_imu param not set.");
        return 1;
    }

    quaternion_publisher = imu_node->create_publisher
                                     <geometry_msgs::msg::QuaternionStamped>
                                     ("orientation", 1);
    euler_publisher = imu_node->create_publisher
                                <robosub_msgs::msg::Euler>
                                ("pretty/orientation", 1);
    acceleration_publisher = imu_node->create_publisher
                                       <geometry_msgs::msg::Vector3Stamped>
                                       ("acceleration/linear", 1);

    rclcpp::Subscription<geometry_msgs::msg::QuaternionStamped>::SharedPtr
                                                               orientation_sub;
    orientation_sub = imu_node->create_subscription
                         <geometry_msgs::msg::QuaternionStamped>
                         (active_imu + "/orientation", 1, orientation_callback);

    rclcpp::Subscription<geometry_msgs::msg::Vector3Stamped>::SharedPtr
                                                              acceleration_sub;
    acceleration_sub = imu_node->create_subscription
                         <geometry_msgs::msg::Vector3Stamped>
                         (active_imu + "/acceleration/linear", 1,
                         acceleration_callback);

    rclcpp::spin(imu_node);

    return 0;
}
