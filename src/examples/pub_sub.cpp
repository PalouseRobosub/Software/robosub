#include <memory>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64.hpp"

rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pub;
/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void convertCallback(const std_msgs::msg::Float64::ConstSharedPtr msg)
{
    std_msgs::msg::Float64 outmsg;

    double degrees = msg->data;
    double radians = degrees * 3.14/180;

    outmsg.data = radians;

    pub->publish(outmsg);
}

int main(int argc, char **argv)
{
    /**
     * The rclcpp::init() function needs to see argc and argv so that it can perform
     * any ROS arguments and name remapping that were provided at the command line.
     * For programmatic remappings you can use a different version of init() which takes
     * remappings directly, but for most command-line programs, passing argc and argv is
     * the easiest way to do it.
     *
     * You must call one of the versions of rclcpp::init()
     * before using any other part of the ROS system.
     */
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("converter");

    /**
     * The create_subscription() call is how you tell ROS that you want to receive messages
     * on a given topic.  This invokes a call to the ROS
     * master node, which keeps a registry of who is publishing and who
     * is subscribing.  Messages are passed to a callback function, here
     * called convertCallback.  create_subscrition() returns a Subscriber object that you
     * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
     * object go out of scope, this callback will automatically be unsubscribed from
     * this topic.
     *
     * The second parameter to the subscribe() function is the size of the message
     * queue.  If messages are arriving faster than they are being processed, this
     * is the number of messages that will be buffered up before beginning to throw
     * away the oldest ones.
     */
    rclcpp::Subscription<std_msgs::msg::Float64>::SharedPtr sub;
    sub = node->create_subscription<std_msgs::msg::Float64>(
                                    "degrees", 1, convertCallback);

    pub = node->create_publisher<std_msgs::msg::Float64>("radians", 1);

    /**
     * rclcpp::spin() will enter a loop, pumping callbacks.  With this version, all
     * callbacks will be called from within this thread (the main one).  ros::spin()
     * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
     */
    rclcpp::spin(node);

    return 0;
}
