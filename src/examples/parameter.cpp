#include <string>
#include <memory>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64.hpp"
#include "std_msgs/msg/string.hpp"

rclcpp::Publisher<std_msgs::msg::String>::SharedPtr pub;


int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("talker");

    pub = node->create_publisher<std_msgs::msg::String>("chatter", 1);


    int loop_frequency;
    if(!node->get_parameter("loop_rate", loop_frequency))
    {
        RCLCPP_INFO(node->get_logger(),
                    "frequency parameter not found, using default");
        loop_frequency = 1;
    }
    RCLCPP_INFO(node->get_logger(), "frequency: %d", loop_frequency);
    rclcpp::Rate rate(loop_frequency);
    while(rclcpp::ok())
    {
        std_msgs::msg::String msg;
        std::string mstr;
        node->get_parameter("message", mstr);
        msg.data = mstr;
        pub->publish(msg);

        rclcpp::spin_some(node);
        rate.sleep();
    }

    return 0;
}
