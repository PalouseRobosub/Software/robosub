#include <signal.h>
#include <memory>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float32.hpp"
#include "utility/test_tools.hpp"

rs::SubscriberAnalyzer<std_msgs::msg::Float32> analyzer;

std::shared_ptr<rclcpp::Node> node;

void sighandler(int sig)
{
    analyzer.Stop();
    RCLCPP_INFO(node->get_logger(), "max : %lf", analyzer.GetMax());
    RCLCPP_INFO(node->get_logger(), "min : %lf", analyzer.GetMin());
    RCLCPP_INFO(node->get_logger(), "avg : %lf", analyzer.GetAverage());
    RCLCPP_INFO(node->get_logger(), "std. dev. : %lf",
                                    analyzer.GetStandardDeviation());

    rclcpp::shutdown();
}

double data_extractor(const std_msgs::msg::Float32::ConstSharedPtr msg)
{
    return msg->data;
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    node = rclcpp::Node::make_shared("data_analyzer_example");

    signal(SIGINT, sighandler);

    analyzer.Init("data", &data_extractor);
    analyzer.Start();

    rclcpp::spin(node);
}
