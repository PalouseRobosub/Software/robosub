#!/usr/bin/env python

import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32


class Node(Node):
    def __init__(self):
        super().__init__('degree_converter')
        self.sub = self.create_subscription(Float32, 'degrees',
                                            self.callback, 1)
        self.pub = self.create_publisher(Float32, 'radians', 1)

    def callback(self, degrees):
        msg = Float32()
        radians = degrees.data * 3.14/180
        msg.data = radians
        self.pub.publish(msg)


if __name__ == "__main__":
    rclpy.init(args=None)
    node = Node()
    rclpy.spin(node)
