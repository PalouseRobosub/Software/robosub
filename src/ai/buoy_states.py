from Base_State import base_state
from robosub_msgs.msg import DetectionArray
from control_wrapper import control_wrapper


# does buoys
class forward_until_buoy(base_state):
    def __init__(self, node):
        self.node = node
        self.done = False

    def vision_callback(self, msg):
        for detection in msg.detections:
            if (detection.label == 'gun' and detection.probability >= 60):
                self.done = True
                self.node.destroy_subscription(self.vision_sub)
                break

    def is_done(self):
        return self.done

    def init(self):
        c = control_wrapper(self.node)
        # arbitratry speed of 0.6
        c.forwardError(0.6)
        c.publish()

        self.vision_sub = self.node.create_subscription(DetectionArray,
                                                        'vision/left',
                                                        self.vision_callback, 1)


# finds and smacks buoy
class smack_buoy(base_state):
    def __init__(self, node):
        self.node = node
        self.done = False

    def is_done(self):
        return self.done

    def vision_callback(self, msg):
        CAMERA_FOV = 60.0  # probably wrong
        CAMERA_FRAME_WIDTH = 1384
        CAMERA_FRAME_HEIGHT = 1032

        c = control_wrapper(self.node)
        for detection in msg.detections:
            if detection.probability >= 60:
                if (detection.label == 'gun'):
                    object_center_x = detection.x + detection.width / 2
                    turn_angle = (((object_center_x / CAMERA_FRAME_WIDTH) *
                                   CAMERA_FOV) - CAMERA_FOV/2)
                    c.yawLeftRelative(turn_angle)

                    object_center_y = detection.y + detection.height / 2

                    # Checking if the target is at the right depth,
                    # if not, dive accordingly
                    if (object_center_y - (CAMERA_FRAME_HEIGHT / 2) > 150):
                        c.diveRelative(1)
                    elif ((CAMERA_FRAME_HEIGHT / 2) - object_center_y > 150):
                        c.diveRelative(-1)

                    self.last_object_seen_time = self.node.get_clock().now()

        c.publish()

    def init(self):
        c = control_wrapper(self.node)
        # arbitratry speed of 0.6
        c.forwardError(0.6)
        c.publish()

        self.last_object_seen_time = self.node.get_clock().now()

        self.vision_sub = self.node.create_subscription(DetectionArray,
                                                        'vision/left',
                                                        self.vision_callback, 1)

    def update(self):
        # done if havent seen object for 15 seconds
        if (self.node.get_clock().now() - self.last_object_seen_time > 15):
            self.done = True
            self.node.destroy_subscription(self.vision_sub)
