from Base_SM import base_sm
import buoy_states
from rclpy.node import Node
import rclpy


class SM_Buoy(base_sm):
    def __init__(self, node):
        self.node = node
        self.done = False

    def init(self):
        self.states = []
        self.state_names = []

        self.states.append(buoy_states.forward_until_buoy(self.node))
        self.state_names.append("forward_until_buoy")

        self.states.append(buoy_states.smack_buoy(self.node))
        self.state_names.append("smack_buoy")

        self.currentStateIndex = 0

        self.states[0].init()

    def is_done(self):
        return self.done


if __name__ == '__main__':
    rclpy.init(args=None)
    node = Node('buoy_sm')
    sm = SM_Buoy(node)
    sm.init()
    while not sm.is_done():
        sm.update()

    rclpy.spin(node)
