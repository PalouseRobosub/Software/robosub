import path_states
from rclpy.node import Node
import rclpy
from CommonStates import basic_forward_state
from Base_SM import base_sm


class SM_path(base_sm):
    def __init__(self, node, forward_duration):
        self.node = node
        self.done = False
        self.forward_duration = forward_duration

    def init(self):
        self.states = []
        self.state_names = []

        self.states.append(path_states.find_marker(self.node))
        self.state_names.append("find_marker")

        self.states.append(path_states.align_with_marker(self.node))
        self.state_names.append("align_with_marker")

        self.states.append(basic_forward_state(0.6, self.forward_duration,
                                               self.node))
        self.state_names.append("forward_to_task")

        self.currentStateIndex = 0

        self.states[0].init()

    def is_done(self):
        return self.done


if __name__ == '__main__':
    rclpy.init(args=None)
    node = Node('path_sm')
    # random duration currently
    sm = SM_path(node, 5)
    sm.init()
    while not sm.is_done():
        sm.update()

    rclpy.spin(node)
