from Base_State import base_state
from robosub_msgs.msg import DetectionArray
from control_wrapper import control_wrapper
from math import atan, degrees


class find_marker(base_state):
    def __init__(self, node):
        self.node = node
        self.done = False

    def vision_callback(self, msg):
        for detection in msg.detections:
            # check for marker name
            if (detection.label == 'path_marker' and
                    detection.probability >= 60):
                self.done = True
                self.node.destroy_subscription(self.vision_sub)
                break

    def is_done(self):
        return self.done

    def init(self):
        c = control_wrapper(self.node)
        # arbitrary speed of 0.6
        c.forwardError(0.6)
        c.publish()

        self.vision_sub = self.node.create_subscription(DetectionArray,
                                                        'vision/bottom',
                                                        self.vision_callback, 1)


# aligns with the marker
class align_with_marker(base_state):
    def __init__(self, node):
        self.node = node
        self.done = False

    def vision_callback(self, msg):
        CAMERA_FRAME_WIDTH = 1384
        CAMERA_FRAME_HEIGHT = 1032

        camera_center_x = CAMERA_FRAME_WIDTH / 2
        camera_center_y = CAMERA_FRAME_HEIGHT / 2

        c = control_wrapper(self.node)
        for detection in msg.detections:
            if (detection.label == "path_marker" and
                    detection.probability >= 60):
                diff_x = camera_center_x - detection.x
                diff_y = camera_center_y - detection.y

                c.forwardError(diff_y / 100)
                c.strafeLeftError(diff_x / 100)

                ratio = detection.height / detection.width
                angle = degrees(atan(ratio))
                c.yawLeftRelative(angle)

                if (ratio > 3):  # random number here
                    self.done = True
                    self.node.destroy_subscription(self.vision_sub)

    def init(self):
        c = control_wrapper(self.node)
        # arbitrary speed of 0.6
        c.forwardError(0.6)
        c.publish()

        self.vision_sub = self.node.create_subscription(DetectionArray,
                                                        'vision/bottom',
                                                        self.vision_callback, 1)

    def is_done(self):
        return self.done
