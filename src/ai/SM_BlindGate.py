# Robot moves through the gate without any cameras, just hardcoded movement

from Base_SM import base_sm
from CommonStates import goto_depth
from CommonStates import basic_forward_state


class SM_BlindGate(base_sm):
    def __init__(self, node):
        self.node = node
        self.done = False

    def init(self):
        self.states = []
        self.state_names = []

        # Starts by diving to depth
        self.states.append(goto_depth(self.node, 2))
        self.state_names.append("goto_depth")

        # Robot goes through gate
        self.states.append(basic_forward_state(1.0, 15, self.node))
        self.state_names.append("basic_forward_state")

        self.currentStateIndex = 0

    def is_done(self):
        return self.done
