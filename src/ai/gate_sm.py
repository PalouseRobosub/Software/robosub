from Base_SM import base_sm
import gate_states
from CommonStates import goto_depth
from rclpy.node import Node
import rclpy


class SM_Gate(base_sm):
    def __init__(self, node):
        self.node = node
        self.done = False

    def init(self):
        self.states = []
        self.state_names = []

        self.states.append(gate_states.take_heading_and_wait(self.node))
        self.state_names.append("take_heading_and_wait")

        # dive to 4 meters

        self.states.append(goto_depth(self.node, 4))
        self.state_names.append("dive_to_gate")

        self.states.append(gate_states.face_gate(self.node))
        self.state_names.append("face_gate")

        self.states.append(gate_states.forward_until_gate(self.node))
        self.state_names.append("forward_until_gate")

        self.states.append(gate_states.go_through_gate(self.node))
        self.state_names.append("go_through_gate")

        self.currentStateIndex = 0

        self.states[0].init()

    def is_done(self):
        return self.done


if __name__ == '__main__':
    rclpy.init(args=None)
    node = Node('gate_sm')
    sm = SM_Gate(node)
    sm.init()
    while not sm.is_done():
        sm.update()

    rclpy.spin(node)
