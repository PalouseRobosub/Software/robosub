# This defines the common actions

import rclpy
import numpy
from Base_State import base_state
from control_wrapper import control_wrapper
from geometry_msgs.msg import QuaternionStamped
from geometry_msgs.msg import Vector3Stamped
from robosub_msgs.msg import Float32Stamped
from scipy.spatial.transform import Rotation as Rot


class basic_forward_state(base_state):
    # Arguments: Forward speed, amount of time to go forward in seconds,
    #            ros node
    def __init__(self, speed, duration, node):
        self.speed = speed
        self.duration = rclpy.duration.Duration(seconds=duration)
        self.node = node
        self.done = False

    def update(self):
        if self.node.get_clock().now() - self.start_time > self.duration:
            c = control_wrapper(self.node)
            c.forwardError(0)
            c.publish()
            self.done = True

    def is_done(self):
        return self.done

    def init(self):
        c = control_wrapper(self.node)
        c.forwardError(self.speed)
        c.publish()
        self.start_time = self.node.get_clock().now()


class basic_turn_state(base_state):
    # Arguments: Relative angle in degrees, ros node
    def __init__(self, turn, node):
        self.node = node
        self.done = False
        self.turn = turn
        self.start_yaw = 0.0
        self.first_yaw = True

    def is_done(self):
        return self.done

    def init(self):
        c = control_wrapper(self.node)
        c.yawLeftRelative(self.turn)
        c.publish()
        self.node.create_subscription(QuaternionStamped, "orientation",
                                      self.heading_callback, 4)

    def heading_callback(self, msg):
        quat = Rot.from_quat(msg.quaternion)
        euler = quat.as_euler('xyz', degrees=True)
        current_yaw = euler[2]
        if self.first_yaw:
            self.start_yaw = current_yaw
            self.first_yaw = False

        difference = current_yaw - self.start_yaw

        if difference >= 87:
            self.done = True


class goto_pinger_state(base_state):
    # Arguments: ros node, forward speed, relative minimum angle in degrees
    #            until the sub decides if it is over the pinger
    def __init__(self, node, forward_speed, angle):
        self.node = node
        self.done = False
        self.forward_speed = forward_speed
        self.angle = angle

    def is_done(self):
        return self.done

    def init(self):
        self.node.create_subscription(Vector3Stamped, "hydrophones/bearing",
                                      self.ping_callback, 4)

    def ping_callback(self, msg):
        v = msg.vector
        ping_yaw = 180/numpy.pi * numpy.arctan(v.y, v.x)
        c = control_wrapper(self.node)
        c.yawLeftRelative(ping_yaw)
        c.levelOut()
        c.setForwardError(self.forward_speed)
        theta_z = 180/numpy.pi * numpy.arctan(numpy.sqrt(v.x**2 + v.y**2),
                                              abs(v.z))
        if theta_z > self.angle:
            self.done = True
            c.setForwardError(0)

        c.publish()


class goto_depth(base_state):
    # Arguments: ros node, dive depth in meters, +- acceptable depth boundary
    def __init__(self, node, depth, max_error=0.2):
        self.node = node
        self.done = False
        self.depth = depth
        self.max_error = max_error

    def is_done(self):
        return self.done

    def init(self):
        c = control_wrapper(self.node)
        c.levelOut()
        c.diveAbsolute(self.depth)
        c.publish()
        self.node.create_subscription(Float32Stamped, "depth",
                                      self.depth_callback, 4)

    def depth_callback(self, msg):
        if abs(abs(msg.data) - abs(self.depth)) < self.max_error:
            self.done = True
