# Parent template for state machines

from Base_State import base_state


class base_sm(base_state):
    def get_current_state_name(self):
        return self.state_names[self.currentStateIndex]

    def update(self):
        if(not self.states[self.currentStateIndex].is_done()):
            self.states[self.currentStateIndex].update()
        else:
            self.currentStateIndex += 1
            if(self.currentStateIndex >= len(self.states)):
                self.done = True
            else:
                self.states[self.currentStateIndex].init()

    def init(self):
        pass

    def is_done(self):
        return self.done
