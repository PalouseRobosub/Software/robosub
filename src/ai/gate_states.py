from Base_State import base_state
from std_msgs.msg import Bool
import rclpy
from robosub_msgs.msg import Euler
from robosub_msgs.msg import DetectionArray
from control_wrapper import control_wrapper


# records initial heading and waits for start switch to be pushed
class take_heading_and_wait(base_state):
    def __init__(self, node):
        self.done = False
        self.node = node

    def start_switch_callback(self, msg):
        self.done = True
        self.node.destroy_subscription(self.start_sub)

    def orientation_callback(self, msg):
        yaw = msg.yaw

        self.node.declare_parameter('gate_heading')
        yaw_parameter = rclpy.Parameter('gate_heading',
                                        rclpy.Parameter.Type.FLOAT64, yaw)
        self.node.set_parameter(yaw_parameter)

        self.start_sub = self.node.create_subscription(
                                                  Bool, 'start_switch',
                                                  self.start_switch_callback, 1)
        self.node.destroy_subscription(self.or_sub)

    def is_done(self):
        return self.done

    def init(self):
        self.or_sub = self.node.create_subscription(Euler, 'pretty/orientation',
                                                    self.orientation_callback,
                                                    1)


# turns towards where gate was
class face_gate(base_state):
    def __init__(self, node):
        self.node = node
        self.done = False

    def heading_callback(self, msg):
        current_yaw = msg.yaw

        difference = abs(current_yaw - self.gate_heading)

        # turn to 3 degrees
        if difference <= 3:
            self.done = True

        self.node.destroy_subscription(self.or_sub)

    def init(self):
        c = control_wrapper(self.node)

        gate_heading_param = self.node.get_parameter('gate_heading')
        self.gate_heading = gate_heading_param.value

        c.yawAbsolute(self.gate_heading)
        c.publish()

        self.or_sub = self.node.create_subscription(Euler, 'pretty/orientation',
                                                    self.orientation_callback,
                                                    1)

    def is_done(self):
        return self.done


# goes blindly forward until gate is seen
class forward_until_gate(base_state):
    def __init__(self, node):
        self.node = node
        self.done = False

    def vision_callback(self, msg):
        for detection in msg.detections:
            if ((detection.label == 'gate' or detection.label == 'gman'
                    or detection.label == 'gangster')
                    and detection.probability >= 60):
                self.done = True
                self.node.destroy_subscription(self.vision_sub)
                break

    def is_done(self):
        return self.done

    def init(self):
        c = control_wrapper(self.node)
        # arbitratry speed of 0.6
        c.forwardError(0.6)
        c.publish()

        self.vision_sub = self.node.create_subscription(DetectionArray,
                                                        'vision/left',
                                                        self.vision_callback, 1)


# use cameras to drive sub through gate
class go_through_gate(base_state):
    def __init__(self, node):
        self.node = node
        self.done = False

    def is_done(self):
        return self.done

    def vision_callback(self, msg):
        CAMERA_FOV = 60.0  # probably wrong
        CAMERA_FRAME_WIDTH = 1384
        CAMERA_FRAME_HEIGHT = 1032

        c = control_wrapper(self.node)
        for detection in msg.detections:
            if detection.probability >= 60:
                if (detection.label == 'gangster' or detection.label == 'gate'
                        or detection.label == 'gman'):
                    object_center_x = detection.x + detection.width/2
                    turn_angle = (((object_center_x/CAMERA_FRAME_WIDTH) *
                                   CAMERA_FOV) - CAMERA_FOV/2)
                    c.yawLeftRelative(turn_angle)

                    object_center_y = detection.y + detection.height/2

                    if (object_center_y - (CAMERA_FRAME_HEIGHT/2) > 150):
                        c.diveRelative(1)
                    elif ((CAMERA_FRAME_HEIGHT/2) - object_center_y > 150):
                        c.diveRelative(-1)

                    self.last_object_seen_time = self.node.get_clock().now()

                    if (detection.label == 'gangster'):
                        break  # ignore other objects if found gangster

        c.publish()

    def init(self):
        c = control_wrapper(self.node)
        # arbitratry speed of 0.6
        c.forwardError(0.6)
        c.publish()

        self.last_object_seen_time = self.node.get_clock().now()

        self.vision_sub = self.node.create_subscription(DetectionArray,
                                                        'vision/left',
                                                        self.vision_callback, 1)

    def update(self):
        # done if havent seen object for 15 seconds
        if (self.node.get_clock().now() - self.last_object_seen_time > 15):
            self.done = True
            self.node.destroy_subscription(self.vision_sub)
