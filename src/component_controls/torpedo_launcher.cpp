#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/bool.hpp>
#include <iostream>
#include <memory>

void fireCallback(const std_msgs::msg::Bool::ConstSharedPtr msg)
{
    if (msg->data) {
        std::cout << "firing" << std::endl;
    }
}


int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> node =
                                 rclcpp::Node::make_shared("torpedo_launcher");

    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr sub;

    sub = node->create_subscription<std_msgs::msg::Bool>(
                                    "fire_torpedoes", 1, fireCallback);

    rclcpp::spin(node);

    return 0;
}
