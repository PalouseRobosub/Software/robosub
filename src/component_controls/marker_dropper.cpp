#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/bool.hpp>
#include <iostream>
#include <memory>

void dropCallback(const std_msgs::msg::Bool::ConstSharedPtr msg)
{
    if (msg->data) {
        std::cout << "dropping the marker" << std::endl;
    }
}


int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> node =
                                 rclcpp::Node::make_shared("marker_dropper");

    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr sub;

    sub = node->create_subscription<std_msgs::msg::Bool>(
                                    "drop_marker", 1, dropCallback);

    rclcpp::spin(node);

    return 0;
}

